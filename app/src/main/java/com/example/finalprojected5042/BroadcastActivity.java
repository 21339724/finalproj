//Name- Jake O'Callaghan
//ID - 21339724

package com.example.finalprojected5042;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import java.text.DateFormat;
import java.util.Calendar;

public class BroadcastActivity extends AppCompatActivity {

    private TextView timeTextView;
    private BroadcastReceiver timeBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);

        timeTextView = findViewById(R.id.time_text_view);
        Button checkTimeButton = findViewById(R.id.check_time_button);

        // Set up the button to log the current time when pressed
        checkTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logCurrentTime();
            }
        });

        // Create a new BroadcastReceiver to receive the ACTION_TIME_CHANGED broadcast
        timeBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Get the current time
                Calendar calendar = Calendar.getInstance();
                String time = DateFormat.getTimeInstance().format(calendar.getTime());
                // Display the time in the TextView
                timeTextView.setText("Current Time: " + time);
            }
        };

        // Register the BroadcastReceiver to receive the ACTION_TIME_CHANGED broadcast
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_TIME_CHANGED);
        registerReceiver(timeBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregister the BroadcastReceiver when the activity is destroyed
        unregisterReceiver(timeBroadcastReceiver);
    }

    private void logCurrentTime() {
        // Get the current time
        Calendar calendar = Calendar.getInstance();
        String time = DateFormat.getTimeInstance().format(calendar.getTime());
        // Log the current time
        Log.d("BroadcastActivity", "Current Time: " + time);

        // Display the current time in a Toast message
        Toast.makeText(this, "Current Time: " + time, Toast.LENGTH_SHORT).show();
    }
}

