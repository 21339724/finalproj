//Name- Jake O'Callaghan
//ID - 21339724

package com.example.finalprojected5042;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.finalprojected5042.R;


public class SQLActivity extends AppCompatActivity {
    private EditText nameEditText, phoneEditText;
    private Button insertButton, viewButton, deleteButton, updateButton;
    private SQLiteDatabase db;

    private void clearFields() {
        nameEditText.setText("");
        phoneEditText.setText("");
    }

    //Update Button - Contacts update through name

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql);

        nameEditText = findViewById(R.id.name_edit_text);
        phoneEditText = findViewById(R.id.phone_edit_text);
        insertButton = findViewById(R.id.insert_button);
        viewButton = findViewById(R.id.view_button);
        deleteButton = findViewById(R.id.delete_button);
        updateButton = findViewById(R.id.update_button);

        db = openOrCreateDatabase("ContactsDB", MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS contacts(name VARCHAR, phone VARCHAR);");

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString().trim();
                String phone = phoneEditText.getText().toString().trim();

                if (name.isEmpty() || phone.isEmpty()) {
                    Toast.makeText(SQLActivity.this, "Please enter all fields", Toast.LENGTH_SHORT).show();
                } else {
                    db.execSQL("INSERT INTO contacts(name, phone) VALUES('" + name + "', '" + phone + "');");
                    Toast.makeText(SQLActivity.this, "Contact added", Toast.LENGTH_SHORT).show();
                    clearFields();
                }
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor c = db.rawQuery("SELECT * FROM contacts", null);
                if (c.getCount() == 0) {
                    Toast.makeText(SQLActivity.this, "No contacts found", Toast.LENGTH_SHORT).show();
                } else {
                    StringBuffer buffer = new StringBuffer();
                    while (c.moveToNext()) {
                        buffer.append("Name: " + c.getString(0) + "\n");
                        buffer.append("Phone: " + c.getString(1) + "\n\n");
                    }
                    showData("Contacts", buffer.toString());
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString().trim();
                if (name.isEmpty()) {
                    Toast.makeText(SQLActivity.this, "Please enter name to delete", Toast.LENGTH_SHORT).show();
                } else {
                    int result = db.delete("contacts", "name=?", new String[]{name});
                    if (result == 0) {
                        Toast.makeText(SQLActivity.this, "No contacts found with that name", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SQLActivity.this, "Contact deleted", Toast.LENGTH_SHORT).show();
                        clearFields();
                    }
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString().trim();
                String phone = phoneEditText.getText().toString().trim();

                if (name.isEmpty() || phone.isEmpty()) {
                    Toast.makeText(SQLActivity.this, "Please enter all fields", Toast.LENGTH_SHORT).show();
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("phone", phone);
                    int result = db.update("contacts", contentValues, "name=?", new String[]{name});
                    if (result == 0) {
                        Toast.makeText(SQLActivity.this, "No contacts found with that name", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SQLActivity.this, "Contact updated", Toast.LENGTH_SHORT).show();
                        clearFields();
                    }
                }
            }
        });
    }

    private void showData(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

}

